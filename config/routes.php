<?php

    return array(
        'product' => 'product/get',
        'category/delete' => 'category/delete',
        'category/edit' => 'category/edit',
        'category/add' => 'category/add',
        'category/([0-9]+)' => 'category/getByID/$1',
        'category' => 'category/get',
        '' => 'home/index',
    );