-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 24 2016 г., 15:53
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Мышки'),
(2, 'Клавиатуры'),
(3, 'Веб камеры');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `photo` varchar(256) NOT NULL,
  `price` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `photo`, `price`, `date`) VALUES
(1, 1, 'A4Tech Bloody V8', 'полноразмерная игровая мышь для ПК, проводная, USB, сенсор оптический 3200 dpi, 8 кнопок, колесо с нажатием', 'aeefc3417f1f00642c2f1e4ec9a463d4.jpg', 328800, '2016-03-20'),
(2, 1, 'Logitech Wireless Mouse M235', 'ноутбучная мышь для ПК, радио, сенсор оптический, 3 кнопки, колесо с нажатием', 'e6aeb987912b3d352b91d046f0d50921.jpg', 303000, '2016-03-18'),
(3, 1, 'Apple Magic Mouse', 'мышь для компьютеров Apple, Bluetooth, сенсор лазерный, колесо сенсорное, цвет белый', '394d2308c7aba173d2bb1ec97b292f4b.jpg', 970200, '2016-03-21'),
(4, 2, 'Logitech Wireless Touch Keyboard K400 Plus Black', 'компактная для ПК/для устройств Android/для планшетов Windows, радио, цвет черный', '890f7763e88587c726546df3a8e67cff.jpg', 588000, '2016-03-22'),
(5, 3, 'Logitech HD Webcam C270 Black', 'матрица 1.3 Мп, кабель 1.5 м', '0d6cc93ca415fafdce68790cba86211f.jpg', 441600, '2016-03-17'),
(6, 3, 'Logitech HD Pro Webcam C920', 'автофокус, кабель 1.8 м, кнопка', '4eb616e1e28c8b478e2a50ec907194f1.jpg', 980600, '2016-03-19');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
