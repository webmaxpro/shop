"use strict";

app.factory('CategoriesService', function($http){
    return {
        getItems: function () {
            return $http({
                method: 'GET',
                url: apiURI + 'category'
            });
        }
    }
});