"use strict";

app.factory('ProductsService', function($http){
    return {
        getItems: function () {
            return $http({
                method: 'GET',
                url: apiURI + 'product'
            });
        }
    }
});