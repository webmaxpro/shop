"use strict";

var app = angular.module('shop', [
    'ui.router'
]);

var apiURI = "http://shop.loc/";

app.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.
        state('home', {
            url: '/',
            templateUrl: "views/app/templates/products.html",
            controller: 'ProductsController as productsCtrl',
            resolve: {
                productsList: function (ProductsService) {
                    return ProductsService.getItems()
                },
                categoriesList: function (CategoriesService) {
                    return CategoriesService.getItems()
                }
            }
        });

        $urlRouterProvider.otherwise('/');
});