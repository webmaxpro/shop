"use strict";

app.controller('CategoriesController', ['$scope','$rootScope','CategoriesService', 'categoriesList', function($scope, $rootScope, CategoriesService, categoriesList){
    this.items = categoriesList.data;
}]);