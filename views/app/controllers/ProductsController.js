"use strict";

app.controller('ProductsController', ['$scope', '$rootScope', 'productsList', 'categoriesList', function($scope, $rootScope, productsList, categoriesList){

    $scope.sortorder = '-date';

    this.productsItems = productsList.data;
    this.categoriesItems = categoriesList.data;

    var selectedCategory = null;
    $scope.selectCategory = function (newCategory) {
        selectedCategory = newCategory;
        setTimeout(setMaxHeightThumbnail);
    };

    $scope.categoryFilterFn = function (produc) {
        return selectedCategory == null ||
            produc.category_id == selectedCategory;
    };

    setTimeout(setMaxHeightThumbnail);
    setTimeout(selectStyle);
}]);

function selectStyle() {
    $('.selectpicker').selectpicker();
}

function setMaxHeightThumbnail() {
    var maxThumbnailHeight = 0;

    $('.thumbnail').each(function(){
        if ($(this).height() > maxThumbnailHeight) { maxThumbnailHeight = $(this).height(); }
    });
    $(".thumbnail").height(maxThumbnailHeight);
}