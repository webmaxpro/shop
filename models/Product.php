<?php

class Product
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    /**
     * Return Product List
     * @return string
     */
    public function getProductList()
    {
        $sql = "SELECT `id`, `category_id`, `name`, `description`, `photo`, `price`, `date` FROM products";

        $result = $this->db->query($sql);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['category_id'] = $row['category_id'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['description'] = $row['description'];
            $products[$i]['photo'] = $row['photo'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['date'] = $row['date'];
            $i++;
        }
        return $products;
    }

    /**
     * Return Product by ID
     * @param integer $id
     * @return string
     */
    public function getProductByID($id)
    {
        $sql = "SELECT `id`, `category_id`, `name`, `description`, `photo`, `price` FROM products WHERE id = :id";

        $result = $this->db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

}