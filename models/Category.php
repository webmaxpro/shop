<?php

class Category
{
    private $db;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    /**
     * Return Categories List
     * @return string
     */
    public function getCategoriesList()
    {
        $sql = "SELECT `id`, `name` FROM categories";

        $result = $this->db->query($sql);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        $categories = array();
        while ($row = $result->fetch()) {
            $categories[$i]['id'] = $row['id'];
            $categories[$i]['name'] = $row['name'];
            $i++;
        }
        return $categories;
    }

    /**
     * Return Category by ID
     * @param integer $id
     * @return string
     */
    public function getCategoryByID($id)
    {
        $sql = "SELECT `id`, `name` FROM categories WHERE id = :id";

        $result = $this->db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        $result->execute();

        return $result->fetch();
    }

    /**
     * @param string $name
     * @return int|string
     */
    public function addCategory($name)
    {
        $sql = "INSERT INTO categories (name) VALUES (:name)";

        $result = $this->db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        if ($result->execute()) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    /**
     * @param string $categoryName
     * @param integer $categoryID
     * @return int|string
     */
    public function editCategory($categoryName, $categoryID)
    {
        $sql = "UPDATE categories SET name = :name WHERE id = :id";

        $result = $this->db->prepare($sql);
        $result->bindParam(':name', $categoryName, PDO::PARAM_STR);
        $result->bindParam(':id', $categoryID, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        if ($result->execute()) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

    /**
     * @param integer $categoryID
     * @return int|string
     */
    public function deleteCategory($categoryID)
    {
        $sql = "DELETE FROM categories WHERE id = :id";

        $result = $this->db->prepare($sql);
        $result->bindParam(':id', $categoryID, PDO::PARAM_INT);

        $result->setFetchMode(PDO::FETCH_ASSOC);

        if ($result->execute()) {
            return $this->db->lastInsertId();
        }
        return 0;
    }

}