<?php

    // Отображаем возможные ошибки для отладки
    ini_set('display_errors',1);
    error_reporting(E_ALL);

    // Подключаем файлы системы
    define('ROOT', dirname(__FILE__));

    require_once(ROOT . '/components/Router.php');
    require_once(ROOT . '/components/Db.php');
    require_once(ROOT . '/components/Request.php');
    require_once(ROOT . '/components/Session.php');

    // Стартуем сессию
    Session::start();

    // Вызов Router
    $router = new Router();
    $router->run();