<?php

include_once ROOT . '/models/Category.php';

class CategoryController
{

    private $categoryModel;
    private $request;

    public function __construct() {
        $this->categoryModel = new Category();
        $this->request = new Request();
    }

    /**
     * Get Categories list
     */
    public function actionGet() {
        $data = $this->categoryModel->getCategoriesList();

        echo json_encode($data);
    }

    /**
     * Get Category by ID
     * @param integer $id
     */
    public function actionGetByID($id) {
        if($id) {
            $data = $this->categoryModel->getCategoryByID($id);

            echo json_encode($data);
        }
    }

    /**
     * Add Category
     */
    public function actionAdd() {
        $categoryName = Request::post('name');
        $data = $this->categoryModel->addCategory($categoryName);

        echo json_encode($data);
    }

    /**
     * Edit Category
     */
    public function actionEdit() {
        $categoryName = Request::post('name');
        $categoryID = Request::post('id');

        $data = $this->categoryModel->editCategory($categoryName, $categoryID);

        echo json_encode($data);
    }

    /**
     * Delete Category
     */
    public function actionDelete() {
        $categoryID = Request::post('id');

        $data = $this->categoryModel->deleteCategory($categoryID);

        echo json_encode($data);
    }

}