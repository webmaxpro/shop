<?php

include_once ROOT . '/models/Product.php';

class ProductController
{

    private $productModel;
    private $request;

    public function __construct() {
        $this->productModel = new Product();
        $this->request = new Request();
    }

    /**
     * Get Product list
     */
    public function actionGet() {
        $data = $this->productModel->getProductList();

        echo json_encode($data);
    }


    /**
     * Get Product by ID
     * @param integer $id
     */
    public function actionGetByID($id) {
        if($id) {
            $data = $this->productModel->getProductByID($id);

            echo json_encode($data);
        }
    }

}